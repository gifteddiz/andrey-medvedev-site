module.exports = {
	head: {
    titleTemplate: 'Nuxt.js',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Meta description' }
    ],
		script: [
      { src: '/js/jquery-3.2.1.min.js' },
      { src: '/js/popper.min.js' },
      { src: '/js/bootstrap.min.js' },
      { src: '/js/masonry.pkgd.min.js' },
      { src: '/js/magnific_popup/jquery.magnific-popup.min.js' }
    ],
    link: [
      { rel: 'stylesheet', href: '/js/magnific_popup/magnific-popup.css' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Montserrat:700,300|Open+Sans:400,400i,700&amp;subset=cyrillic-ext' }
    ]
  },
  mode: 'spa',
  css: [
    { src: '@/assets/scss/main.scss', lang: 'sass' }
  ]
}