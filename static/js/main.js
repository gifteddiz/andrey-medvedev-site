function initMobileMenu(){
  if($(window).width() < 768){
    $('.jsMobileMenuToggler').on('touchstart', function (e) {
      e.preventDefault();
      $(this).toggleClass('menu-is-active');
      $('.panel__nav').toggleClass('panel__nav--visible');
    });
  }
}
$(initMobileMenu);

function pageLoaded(){
  document.getElementById('video').play();
  $('.loader').fadeOut(600);
  setTimeout(function(){ $('.intro__title').addClass('-animated');},200);
  setTimeout(function(){ $('.intro__message').addClass('-animated');},300);
  setTimeout(function(){ $('.intro__skype').addClass('-animated');},400);
  setTimeout(function(){ $('.intro__phone').addClass('-animated');},500);
  setTimeout(function(){ $('.intro__name').addClass('-animated');},300);
  setTimeout(function(){ $('.intro__scroll').addClass('-animated');},400);
}
$(window).on('load', pageLoaded);

function initWorks(){
  $('.works__wrapper').masonry({
    itemSelector: '.works__item',
    percentPosition: true
  })
}
$(window).on('load', initWorks);

function initParallax(){
  $("[data-paroller-factor]").paroller();
}
$(window).on('load', function(){
  if( $(window).width() > 767) {
    initParallax();
  }
});

function initWorkView(){
  $('body').append('<div class="overlay"></div>')
  $('.works__item').click(function(e){
    e.preventDefault();
    var eTop = $(this).offset().top - $(window).scrollTop(),
        eLeft = $(this).offset().left,
        width = $(this).width(),
        height = $(this).height(),
        url = $(this).attr('href');

    if( !$('.overlay__item').length ) $('.overlay').append('<div class="overlay__item"></div>');
    $('.overlay__item').css({
      'top': eTop,
      'left': eLeft,
      'width': width,
      'height': height
    });
    $('.overlay__item').css({
      'top': 0,
      'left': 0,
      'width': $(window).width(),
      'height': $(window).height()
    });

    $.magnificPopup.open({
      items: {
        src: url
      },
      type: 'image',
      removalDelay: 300,
      mainClass: 'my-mfp-zoom-in',
      image: {
        verticalFit: false
      },
      callbacks: {
        close: function(){
          $('.overlay__item').remove();
        }
      }
    }, 0);
  })
}
$(initWorkView);

function initSmoothScroll(){
  $('.jsSmoothScroll').click(function() {
    var target = $(this.hash);

    $('.jsMobileMenuToggler').removeClass('menu-is-active');
    $('.panel__nav').removeClass('panel__nav--visible');
    if( $(window).width() > 767 ){
      $('html, body').animate({
        scrollTop: target.offset().top
      }, 1000);
      return false;
    }
  });
}
$(initSmoothScroll);

function initScrollSpy(){
  var points = [];
  $('.panel__nav-item').each(function(){
    if( $( $(this).attr('href')).length ){
      points.push( $( $(this).attr('href') ).offset().top );
    }
  })
  $(window).on('scroll', function(){
    var scrollPos = $(window).scrollTop() + $(window).height()-100,
        current = 0;

    $.each(points, function(ind){
      var val = parseInt(this);
      if(scrollPos > val) current = ind;
    })
    $('.panel__nav-item').eq(current).addClass('panel__nav-item--active').siblings().removeClass('panel__nav-item--active');
  })
}
$(window).on('load',initScrollSpy);